Coding exercise for the Senior Frontend Developer position.


The goal of the exercise is to make the application working and make it as production ready as you can.



You need to create a simple React (or Vue) application that lists a set of documents. It is a big plus if you can do the test in Vue.



The list page should include a filter and sorting functionality



You should only show documents with the format .pdf and .docx.



Date format: dd-mm-yyyy



Filter by date (from - to) like in annex.



Sort by name or date (default by date, new first).


Layout should be responsive (mobile, tablet, desktop). All designs and style guides attached.